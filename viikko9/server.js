const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + '/public'));

// Main page, logs database result
app.get('/', (req, res) => {
	console.log('Main page');
	res.sendFile(__dirname + '/index.html');
	db.collection('todos').find().toArray((err, result) => {
		console.log(result);
	});
});
//load todos from database
app.get('/load', (req, res) => {
	console.log('load from database');
	db.collection('todos').find({}, {_id: 0}).toArray((err, result) => {
		res.send(result);
	});
});

// Add new todo
app.post('/add', (req, res) => {
	console.log(req.body.text);
	db.collection('todos').insertOne({text: req.body.text}, (err, result) => {
    if (err) return console.log(err)
    	res.send('Added to DB');
  });
})

// Delete todo
app.post('/delete', (req, res) => {
	let task = req.body.text;
	console.log(task);
	db.collection('todos').findOneAndDelete({text : task}, (err, result) => {
    if (err) return console.log(err) 
    	res.send('Deleted from DB');
  })
})

// MongoDB connect + PORT listener
const MongoClient = require('mongodb').MongoClient
var db
MongoClient.connect('mongodb://192.168.99.100:27017/viikko9', (err, client) => {
	if (err) return console.log(err)
	db = client.db('todolistdb') // database name
	db.createCollection('todos')
	app.listen(3000, () => {
	console.log('listening on 3000')
	})
});
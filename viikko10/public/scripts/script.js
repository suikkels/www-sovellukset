
Vue.component('list-item', {
  template: '\
  	<li class="collection-item">\
  		<div>{{ title }}<a href = "#!" class="secondary-content">\
  		<i class="material-icons icon-red" v-on:click="$emit(\'remove\')">clear</i></a></div>\
  	</li>\
  	 ',
  	 props: ['title', 'index']
});

var app = new Vue({
	el: '#app',

	data: {
		newItemText: '',
		items: [],
	},
	mounted() {
    	console.log("App mounted");
    	this.updateList();
  	},

	methods: {
		addItem(){

			if (!this.validText(this.newItemText)) {
				return 0;
			} else {
				console.log(this.newItemText);
				axios.post('/add', {text : app.newItemText})
				.then(function (response) {
					console.log(response);
				})
				.catch(function (error) {
					console.log(error);
				});
				this.updateList();
			}
		},

		removeFromList(index) {

			console.log(index);

			axios.post('/delete', {text : index})
			.then(function (response) {
				console.log(response);
			})
			.catch(function (error) {
				// handle error
				console.log(error);
			});
			this.updateList();

		},

		validText: function(itemText){
			var re = /^([a-zA-Z\-0-9\-,.]+)$/;
			return re.test(itemText);
		},

		updateList() {
			axios.get('/load')
			.then(function (response) {
				app.items = [];
				for (var i = 0; i < response.data.length; i++) {
					app.items.push(response.data[i]);
				}
				console.log(app.items);

			})
			.catch(function (error) {
				// handle error
				console.log(error);
				})
		},
	}

});

Vue.component('error-text', {
  template: '\
  	<div>\
  	 	{{ title }}\
  	 </div>\
  	',
  	 props: ['title']
});


var signapp = new Vue({
	el: '#signapp',

	data: {
		email: '',
		password: '',
		error: ''
	},
	mounted() {
    	console.log("signing up");
  	},

	methods: {
		signup(){
			//console.log(signapp.email);
			axios.post('/signup', {email : signapp.email, password : signapp.password})
			.then(function (response) {
				console.log(response);
				if(response.data.success){
					signapp.error = '';
					window.location.href = "/profile";
				} else {
					signapp.error = "email already in use";
				}
			})
			.catch(function (error) {
				console.log(error);
			});
		},
	}
});
Vue.component('error-text', {
  template: '\
  	<div>\
  	 	{{ title }}\
  	 </div>\
  	',
  	 props: ['title']
});

var loginapp = new Vue({
	el: '#loginapp',

	data: {
		email: '',
		password: '',
		error: ''
	},
	mounted() {
    	console.log("logging in...");
  	},

	methods: {
		login(){
			//console.log(loginapp.email);
			axios.post('/login', {email : loginapp.email, password : loginapp.password})
			.then(function (response) {
				console.log(response);
				if(response.data.success){
					loginapp.error = '';
					window.location.href = "/profile";
				} else {
					loginapp.error = "Wrong email or password";
				}
			})
			.catch(function (error) {
				console.log(error);
			});
		},
	}
});

var Todo = require('./models/todo');

module.exports = function(app, passport) {

    // =====================================
    // HOME PAGE (with login links) ========
    // =====================================
    app.get('/', function(req, res) {
        res.render('index.ejs'); // load the index.ejs file
    });

    // =====================================
    // LOGIN ===============================
    // =====================================
    // show the login form
    app.get('/login', function(req, res) {

        // render the page and pass in any flash data if it exists
        res.render('login.ejs', { message: req.flash('loginMessage') }); 
    });

    // process the login form
    app.post('/login', function(req, res, next) {
        passport.authenticate('local-login', function(err, user) {

            if (err) {
              return next(err); // will generate a 500 error
            }
            if (!user) {
                return res.send({ success : false, message : 'authentication failed' });
            }
            // Generate a JSON response reflecting authentication status
            req.login(user, loginErr => {
                if (loginErr) {
                    return next(loginErr);
                }
                res.send({success : true, message : 'authentication succesful'});
            });
        })(req,res,next);
    });

    // =====================================
    // SIGNUP ==============================
    // =====================================
    // show the signup form
    app.get('/signup', function(req, res) {

        // render the page and pass in any flash data if it exists
        res.render('signup.ejs', { message: req.flash('signupMessage') });
    });

    // process the signup form
    app.post('/signup', function(req, res, next) {
        passport.authenticate('local-signup', function(err, user) {
            if (err) {
              return next(err); // will generate a 500 error
            }
            if (!user) {
                return res.send({ success : false, message : 'user already exists' });
            }
            // Generate a JSON response reflecting authentication status
            req.login(user, loginErr => {
                if (loginErr) {
                    return next(loginErr);
            }
            res.send({success : true, message : 'signup succesful'});
            });
        })(req,res,next);

    });


    // =====================================
    // PROFILE SECTION =====================
    // =====================================
    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this (the isLoggedIn function)
    app.get('/profile', isLoggedIn, function(req, res) {
        res.render('profile.ejs');
        Todo.find((err, result) => {
            console.log(result);
        });
    });

    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    //load todos from database
    app.get('/load', (req, res) => {
        console.log('load from database');
        Todo.find({}, {_id: 0}, (err, result) => {
            res.send(result);
        });
    });

    // Add new todo
    app.post('/add', (req, res) => {
        console.log(req.body.text);
        var newTodo = new Todo();
        newTodo.text = req.body.text
        newTodo.save(function(err, result) {
        if (err) 
            return console.log(err);
        res.send('Added to DB');
      });
    });

    // Delete todo
    app.post('/delete', (req, res) => {
        let task = req.body.text;
        console.log(task);
        Todo.findOneAndRemove({text : task}, (err, result) => {
        if (err) return console.log(err) 
            res.send('Deleted from DB');
      });
    });
};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
};
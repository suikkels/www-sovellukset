var peli = {
    taulu:   [[0,0,0],
            [0,0,0],
            [0,0,0]],
    
    pelaajaVuoro: 1
};

function onkoSuora(pelitaulu) {
    var sarake1 = [pelitaulu[0][0], pelitaulu[1][0], pelitaulu[2][0]];
    var sarake2 = [pelitaulu[0][1], pelitaulu[1][1], pelitaulu[2][1]];
    var sarake3 = [pelitaulu[0][2], pelitaulu[1][2], pelitaulu[2][2]];
    var vino1 = [pelitaulu[0][0], pelitaulu[1][1], pelitaulu[2][2]];
    var vino2 = [pelitaulu[0][2], pelitaulu[1][1], pelitaulu[2][0]];
    var rivi1 = [pelitaulu[0][0], pelitaulu[0][1], pelitaulu[0][2]];
    var rivi2 = [pelitaulu[1][0], pelitaulu[1][1], pelitaulu[1][2]];
    var rivi3 = [pelitaulu[2][0], pelitaulu[2][1], pelitaulu[2][2]];
    
    if ((sarake1[0] == 1 && sarake1[1] == 1 && sarake1[2] == 1) || (sarake1[0] == 2 && sarake1[1] == 2 && sarake1[2] == 2)) {
        if(sarake1[0] == 1) {
            return 1;
        } else {
            return 2;
        }
    } else if((sarake2[0] == 1 && sarake2[1] == 1 && sarake2[2] == 1) || (sarake2[0] == 2 && sarake2[1] == 2 && sarake2[2] == 2)) {
        if(sarake2[0] == 1) {
            return 1;
        } else {
            return 2;
        }
    } else if((sarake3[0] == 1 && sarake3[1] == 1 && sarake3[2] == 1) || (sarake3[0] == 2 && sarake3[1] == 2 && sarake3[2] == 2)) {
        if(sarake3[0] == 1) {
            return 1;
        } else {
            return 2;
        }
    } else if((vino1[0] == 1 && vino1[1] == 1 && vino1[2] == 1) || (vino1[0] == 2 && vino1[1] == 2 && vino1[2] == 2)) {
        if(vino1[0] == 1) {
            return 1;
        } else {
            return 2;
        }
    } else if((vino2[0] == 1 && vino2[1] == 1 && vino2[2] == 1) || (vino2[0] == 2 && vino2[1] == 2 && vino2[2] == 2)) {
        if(vino2[0] == 1) {
            return 1;
        } else {
            return 2;
        }
    } else if((rivi1[0] == 1 && rivi1[1] == 1 && rivi1[2] == 1) || (rivi1[0] == 2 && rivi1[1] == 2 && rivi1[2] == 2)) {
        if(rivi1[0] == 1) {
            return 1;
        } else {
            return 2;
        }
    } else if((rivi2[0] == 1 && rivi2[1] == 1 && rivi2[2] == 1) || (rivi2[0] == 2 && rivi2[1] == 2 && rivi2[2] == 2)) {
        if(rivi2[0] == 1) {
            return 1;
        } else {
            return 2;
        }
    } else if((rivi3[0] == 1 && rivi3[1] == 1 && rivi3[2] == 1) || (rivi3[0] == 2 && rivi3[1] == 2 && rivi3[2] == 2)) {
        if(rivi3[0] == 1) {
            return 1;
        } else {
            return 2;
        }
    } else {
        return 0;
    }

};

function onkoTaysi(pelitaulu) {
    for(var i = 0; i < pelitaulu.length; i++) {
        for(var j = 0; j < pelitaulu[i].length; j++) {
            if(pelitaulu[i][j] == 0) {
                return 0;
            } 
        }
    }
    return 1;
}

function asetaRuutu(ruutu, sarake, rivi, pelidata) {
    
    if(document.getElementById(ruutu).innerHTML == 'X' || document.getElementById(ruutu).innerHTML == 'O') {
        
        return 0;
    }

    pelidata.taulu[sarake][rivi] = pelidata.pelaajaVuoro;
    if(pelidata.pelaajaVuoro == 1) {
        document.getElementById(ruutu).innerHTML = 'X';
        pelidata.pelaajaVuoro = 2;
        document.getElementById("vuoroinfo").innerHTML = "Vuoro: O";
    } else if(pelidata.pelaajaVuoro == 2) {
        document.getElementById(ruutu).innerHTML = 'O';
        document.getElementById("vuoroinfo").innerHTML = "Vuoro: X";
        pelidata.pelaajaVuoro = 1;
    }

    if(onkoSuora(pelidata.taulu) == 1 ) {
        document.getElementById("infotext").innerHTML = "X winner winner chicken dinner";
    } else if(onkoSuora(pelidata.taulu) == 2) {
        document.getElementById("infotext").innerHTML = "O winner winner chicken dinner";
    } else if(onkoTaysi(pelidata.taulu) == 1) {
        document.getElementById("infotext").innerHTML = "draw";
    }


    console.log(pelidata.taulu[sarake][rivi], pelidata.pelaajaVuoro);

    return 0;
}
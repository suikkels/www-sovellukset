function onkoSuora(pelitaulu) {
    var sarake1 = [pelitaulu[0][0], pelitaulu[1][0], pelitaulu[2][0]];
    var sarake2 = [pelitaulu[0][1], pelitaulu[1][1], pelitaulu[2][1]];
    var sarake3 = [pelitaulu[0][2], pelitaulu[1][2], pelitaulu[2][2]];
    var vino1 = [pelitaulu[0][0], pelitaulu[1][1], pelitaulu[2][2]];
    var vino2 = [pelitaulu[0][2], pelitaulu[1][1], pelitaulu[2][0]];
    var rivi1 = [pelitaulu[0][2], pelitaulu[0][1], pelitaulu[0][2]];
    var rivi2 = [pelitaulu[1][0], pelitaulu[1][1], pelitaulu[1][2]];
    var rivi3 = [pelitaulu[2][0], pelitaulu[2][1], pelitaulu[2][2]];
    
    if ((sarake1[0] == 1 && sarake1[1] == 1 && sarake1[2] == 1) || (sarake1[0] == 2 && sarake1[1] == 2 && sarake1[2] == 2)) {

        return 1;
    } else if((sarake2[0] == 1 && sarake2[1] == 1 && sarake2[2] == 1) || (sarake2[0] == 2 && sarake2[1] == 2 && sarake2[2] == 2)) {

        return 1;
    } else if((sarake3[0] == 1 && sarake3[1] == 1 && sarake3[2] == 1) || (sarake3[0] == 2 && sarake3[1] == 2 && sarake3[2] == 2)) {

        return 1;
    } else if((vino1[0] == 1 && vino1[1] == 1 && vino1[2] == 1) || (vino1[0] == 2 && vino1[1] == 2 && vino1[2] == 2)) {

        return 1;
    } else if((vino2[0] == 1 && vino2[1] == 1 && vino2[2] == 1) || (vino2[0] == 2 && vino2[1] == 2 && vino2[2] == 2)) {

        return 1;
    } else if((rivi1[0] == 1 && rivi1[1] == 1 && rivi1[2] == 1) || (rivi1[0] == 2 && rivi1[1] == 2 && rivi1[2] == 2)) {

        return 1;
    } else if((rivi2[0] == 1 && rivi2[1] == 1 && rivi2[2] == 1) || (rivi2[0] == 2 && rivi2[1] == 2 && rivi2[2] == 2)) {

        return 1;
    } else if((rivi3[0] == 1 && rivi3[1] == 1 && rivi3[2] == 1) || (rivi3[0] == 2 && rivi3[1] == 2 && rivi3[2] == 2)) {

        return 1;
    } else {
        return 0;
    }

};
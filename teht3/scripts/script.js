Vue.component('list-item', {
  template: '\
  	<li>\
  	 {{ title }}\
  	 <button v-on:click="$emit(\'remove\')">Remove</button>\
  	 </li>\
  	 ',
  	 props: ['title']
});

var vm = new Vue({
	el: '#app',

	data: {
		newItemText: '',
		items: [
			{
				id: 1,
				title: 'asd'
			},
			{
				id: 2,
				title: 'dsa'
			}
		],
		nextItemId: 3
	},
	mounted() {
    	if (localStorage.newItemText) {
      		this.newItemText = localStorage.newItemText;
    	}
  	},
  	watch: {
    	newItemText(newNewItemText) {
      		localStorage.newItemText = newNewItemText;
    	}
  	},
	methods: {
		addItem: function (){
			if (!this.validText(this.newItemText)) {
				return 0;
			} else {
				this.items.push({
					id: this.nextItemId++,
					title: this.newItemText

				})
			}
			this.newItemText = ''
		},

		validText: function(itemText){
			var re = /^([a-zA-Z\-0-9\-,.]+)$/;
			return re.test(itemText);
		}
	}

});

var express = require('express');
var app = express();
var mongoose = require('mongoose');
var passport = require('passport');

var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

// set database url and connect to it
var configDB = require('./config/database.js');
// for actual app
mongoose.connect(configDB.url);
// for unittesting
//mongoose.connect(configDB.uniturl);

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms

//set static files
app.use(express.static(__dirname + '/public'));

//set view engine templates
app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({ secret: 'www-sovellukset' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

//routes
require('./app/routes.js')(app, passport);

module.exports = app;
## WWW-sovellukset harjoitustyö
Tekijä: Saku Suikkanen (0504774)

Työn aihe ei ollut mitenkään hartaasti harkittu, vaan yksittäinen ajatus, jota lähdin toteuttamaan. Pohjimmiltaan kuitenkin toteutus eteni ajatusmallilla: ensin näkymä (html), toiseksi modelit (mongoose) ja lopuksi toteutus (javascript). Sitten pikkuhiljaa lisäiltiin uusia elementtejä, kuten kuvaajat ja kommentit.

### Koodin ajaminen
Ohjelmaa on testattu Windows 10-käyttöjärjestelmässä käyttäen Docker Toolbox-työkalua konttien ajamiseen.
Ohjelma ja tietokanta käynnistyvät komennolla: ”docker-compose up”, jolloin ohjelma näkyy osoitteessa: http://localhost:3000.

Kontit voi buildata myös suoraan repositoriosta luomalla tiedoston docker-compose.yml:

	version: '3'

	services:

		app:
			container_name: harkkatyo
			restart: always
			build: https://bitbucket.org/suikkels/www-sovellukset.git#master:HT
			ports:
				- '3000:3000'
			links:
				- mongo
			depends_on:
				- mongo
		mongo:
			container_name: mongo
			image: mongo
			ports:
				- '27017:27017'
 
### Yksikkötestaus
Työ sisältää 5 kappaletta erilaisia yksikkötestejä:
- kommenttien haku
- tunnusten luonti
- pisteiden tallennus
- aloitussivun testaus
- tietokannan yhteyden testaus

Testien onnistuneeksi ajamiseksi täytyy luoda erillinen tietokanta esim. komennolla:

> docker run -d -p 27017:27017 mongo
 
Tämän jälkeen täytyy kommentoida app.js-tiedoston rivi 14:
> mongoose.connect(configDB.url); --> //mongoose.connect(configDB.url);
 
Ja "epäkommentoida" rivi 16:
> //mongoose.connect(configDB.uniturl); --> mongoose.connect(configDB.uniturl);
 
Tämän jälkeen testit voidaan ajaa komennolla:
npm test

### Työn sisältö
Työ on jaettu selkeästi muutamiin alla lueteltuihin kansioihin/tiedostoihin.

#### /app
App-kansio sisältää alikansion models, sekä javascript tiedoston routes.js. Kansiossa models, sijaitsee mongoose skeemat comment, score ja user. Tiedosto routes.js sisältää routet mm. kirjautumiseen, tunnusten luontiin, ja facebook-autentikaatioon.

#### /config
Kansio config sisältää konfiguraatiotiedostot auth.js, database.js ja passport.js. Auth.js sisältää JSON-olion facebook-autentikointia varten, database.js vastaavasti eri tietokantoja varten ja passport.js sisältää toiminnallisuuden käyttäjien autentikointia varten.

#### /public
Public-kansio sisältää staattisia tiedostoja ohjelman ulkoasuun ja toiminnallisuuteen liittyen. Scripts.js-kansio sisältää enimmäkseen Vue-kontrollereita html-formeja varten, mutta game.js sisältää myös game-näkymän pelin lähdekoodin. Styles-kansiossa on sivun muotoiluun liittyvät tiedostot ja assets-kansio sisältää pelin staattisia kuvatiedostoja.

#### /views
Kansio sisältää view enginen ejs-tiedostot eri näkymiä varten.

#### /tests
Sisältää yhden tiedoston app.test.js, joka sisältää yksikkötestit.

#### app.js
Tiedosto sisältää ohjelmassa tarvittavien elementtien, kuten express.js ja mongoosen määrittelyt ja yhdistää nämä.

#### server.js
Sisältää ainoastaan funktion portin kuuntelulle ja on erotettu app.js-tiedostosta yksikkötestien takia.

### Uuden käyttäjän autentikointi
Uuden käyttäjän voi luoda aloitussivun "local signup"-painikkeesta, jolloin käyttäjä ohjataan "/signup"-routeen. Avautuvasta näkymästä käyttäjä kykenee luomaan uuden käyttäjän sivustolle antamalla sähköpostiosoitteen ja salasanan, sekä painamalla "signup"-painiketta. Tiedostossa signup.js sijaitsee Vue-kontrolleri signapp, jonka avulla html-formiin syötetty tieto tallennetaan signapp-kontrolleriin ja lähetetään AJAX kutsulla routeen POST(/signapp). Routessa uusi käyttäjä autentisoidaan käyttäen passport.js kirjaston local strategya ja lisäksi salasana suojataan "hasheilla", sekä suolauksella, käyttäen bcrypt-node kirjastoa. Onnistuneen autentisoinnin jälkeen luodaan User-olio, joka tallennetaan tietokantaan ja käyttäjä ohjataan routeen GET(/catalog).
Facebook-autentikointi
Palveluun voidaan myös kirjautua olemassa olevan Facebook-käyttäjän avulla. Facebook-autentikointi aloitetaan painamalla aloitussivulla "facebook"-painiketta, jolloin käyttäjä ohjataan facebookin autentikaatiosivulle johon syötetään käyttäjätunnus ja salasana. Onnistuneen autentikoinnin jälkeen Facebookin API lähettää takaisin käyttäjän tietoja, kuten nimen ja sähköpostiosoitteen, sekä käyttäjä ohjataan onnistuneesti catalog-näkymään.
Palveluun kirjautuminen
Palveluun voi kirjautua olemassa olevalla lokaalilla käyttäjällä, index-näkymän "local login"-painikkeesta. Tällöin täytetty html-form lähetetään loginapp Vue-kontrollerin avulla autentikoitavaksi routeen GET(/login). Routessa palvelin autentikoi syötetyn käyttäjän, käyttäen passport.js:än local strategya ja etsii tietokannasta autentikaation mukaista sähköpostia, sekä salasanaa. Onnistuneen autentikaation jälkeen käyttäjä ohjataan "catalog"-näkymään.
 
### Pelin valitseminen ja pelaaminen
Catalog-näkymästä voidaan valita haluamamme peli valikosta, jokseenkin vain yksi on käytettävissä. Pelin valittuaan kutsutaan routea GET("/game"), joka renderöi näkymän "game.ejs". Näkymässä sijaitsee keskellä ruutua yksinkertainen phaser-framworkilla kehitetty canvas-pohjainen peli, jota käyttäjä pystyy ohjailemaan vasen-, oikea-nuolinäppäimellä, sekä control-painikkeella. Pelin vasemmassa yläkulmassa näkyvät pisteet, jotka päivittyvät jokaisesta osumasta ja oikeassa yläkulmassa taasen sijaitsee lukema jäljellä olevistä elämistä. Kyseisen lukeman mennessä nollaan, lähetetään AJAX-kutsun avulla pistetilanne, sekä sen hetkinen käyttäjä routeen POST(/addscore), missä tiedoista luodaan uusi Score-olio, joka tallennetaan tietokantaan. Alempana sivulla, käyttäjä pystyy myös kommentoimaan peliä kirjoittamalla kommentin tekstikenttään ja painamalla "comment"-painiketta. Kommentti lähetetään commentapp Vue-kontrollerin avulla AJAX-kutsulla routeen POST(/addcomment), jossa lähetetyistä tiedoista luodaan Comment-olio ja lisätään tietokantaan. Jokaisen onnistuneen lisäyksen sekä sivun päivityksen jälkeen päivitetään kaikki kommentit näkymään. Kommenttien alapuolelta löytyy pisteiden top10-tilanne, joka on visualisoitu käyttäen chart.js-frameworkia. Tilanne päivitetään aina näkymän latauksen yhteydessä, jolloin gameapp Vue-kontrolleri hakee tiedot GET(/loadscore)-routesta, joka järjestää Score-oliot pistemäärien mukaan ja palauttaa 10 suurinta.

### Yleistä
Catalog- ja game-näkymät sisältävät yläreunassa navbar-elementin, jossa oikeassa reunassa sijaitsee painike "logout", josta käyttäjä voi kirjautua ulos palvelusta, sekä painike "catalog", josta käyttäjä ohjataan catalog-näkymään. Vaihtoehtoisesti, keskellä elementtiä sijaitsee myös teksti-elementti: "UGS9000", jota painamalla päästään catalog-sivulle. Kaikki näkymät ovat myös responsiivisia, joten niitä pitäisi pystyä helposti tulkitsemaan laitteella kuin laitteella. Työ on myös tehty MVC-mallin mukaan, ja modelit löytyvät osoitteesta /app/models, viewit /views ja controllerit /public/scripts. Kansiossa scripts, controllereja ovat comment.js, games.js, login.js, signup.js ja user.js ja ne ovat toteutettu Vue-kirjaston avulla.
Toteutetut palikat:

- responsiivinen ulkoasu (+5)
- tietokannan käyttö (+5)
- ORM:n (mongoose) (+2)
- Käyttäjän autentikointi (+5)
- Front-controlleri (Vue esim. comment.js, game.js jne.) (+5)
- MVC-malli (+2)
- Front-näkymät (Vuen komponentit esim. comment.js) (+2)
- Kolmannen osapuolen palvelu (FB-login) (+5)
- Yksikkötestit (+5)
- JSONin käyttö (esim. auth.js, tietokanta haut) (+3)
- Dynaaminen SVG-grafiikka (game.js: chart.js) (+3)
- Canvas-elementti (game.js: chart, phaser) (+3)
- AJAX-ohjelmointi (esim. game.js login.js, signup.js jne.) (+3)
- Dockerin käyttö useammalla kontilla (web + tietokanta) (+3)

Lisäksi mahdolliset pisteet kattavasta dokumentoinnista ja vertaisarviosta (+10)
YHT: 50-60 pts

const app = require('./app.js');
// set port
var port = process.env.PORT || 3000;
//open port
app.listen(port, () => {
	console.log('listening on ' + port);
});
"use strict";

var Comment = require('./models/comment');
var Score = require('./models/score');

module.exports = function(app, passport) {

	// =====================================
    // HOME PAGE (with login links) ========
    // =====================================
    app.get('/', function(req, res) {
        res.render('index.ejs'); // load the index.ejs file
    });

    // =====================================
    // LOGIN ===============================
    // =====================================
    // show the login form
    app.get('/login', function(req, res) {

        // render login.ejs
        res.render('login.ejs'); 
    });

    // process the login form
    app.post('/login', function(req, res, next) {
        passport.authenticate('local-login', function(err, user) {

            if (err) {
              return next(err); // will generate a 500 error
            }
            if (!user) {
            	// send failed response to controller
                return res.send({ success : false, message : 'authentication failed' });
            }
            // Generate a JSON response reflecting authentication status
            req.login(user, loginErr => {
                if (loginErr) {
                    return next(loginErr);
                }
                // send succesful response to controller
                res.send({user, success : true, message : 'authentication succesful'});
            });
        })(req,res,next);
    });

    // =====================================
    // SIGNUP ==============================
    // =====================================
    // show the signup form
    app.get('/signup', function(req, res) {

        // render signup.ejs
        res.render('signup.ejs');
    });

    // process the signup form
    app.post('/signup', function(req, res, next) {
        passport.authenticate('local-signup', function(err, user) {
            if (err) {
              return next(err); // will generate a 500 error
            }
            if (!user) {
            	// send failed response to controller
                return res.send({ success : false, message : 'user already exists' });
            }
            // Generate a JSON response reflecting authentication status
            req.login(user, loginErr => {
                if (loginErr) {
                    return next(loginErr);
            }
            // send succesful response to controller
            res.send({user, success : true, message : 'signup successful'});
            });
        })(req,res,next);

    });

    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });


    // =====================================
    // CATALOG PAGE ========================
    // =====================================
    app.get('/catalog', isLoggedIn, function(req, res) {
        res.render('catalog.ejs'); // load the catalog.ejs file
    });

	// =====================================
    // GAME PAGE ===========================
    // =====================================
    app.get('/game', isLoggedIn, function(req, res) {
        res.render('game.ejs'); // load the game.ejs file
    });

	// =====================================
    // LOAD COMMENTS =======================
    // =====================================
    app.get('/loadcomments', (req, res) => {
        console.log('load from database');
        Comment.find({}, {_id: 0}, (err, result) => {
            res.send(result);
        });
    });

    // =====================================
    // ADD COMMENT =========================
    // =====================================
    app.post('/addcomment', (req, res) => {
        console.log(req.body.text);
        //create new comment
        var newComment = new Comment();
        //set comment parameters
        newComment.text = req.body.text;
        newComment.user = req.body.user;
        // save to database
        newComment.save(function(err, result) {
        if (err) 
            return console.log(err);
        res.send('Added to DB');
      });
    });

    // =====================================
    // ADD SCORE ===========================
    // =====================================
    app.post('/addscore', (req, res) => {
        console.log(req.body);
        //create new score
        var newScore = new Score();
        //set score parameters
        newScore.user = req.body.user
        newScore.score = req.body.score
        // save to database
        newScore.save(function(err, result) {
        if (err) 
            return console.log(err);
        res.send(result);
      });
    });

   	// =====================================
    // LOAD SCORES =========================
    // =====================================
    app.get('/loadscore', (req, res) => {
        console.log('load from database');
        Score.find({}, {_id: 0}).sort({score: 'descending'}).limit(10).exec(function(err, result) {
            res.send(result);
        });
    });

    // =====================================
    // FACEBOOK ROUTES =====================
    // =====================================
    // route for facebook authentication and login
    app.get('/auth/facebook', passport.authenticate('facebook', {
      scope : ['public_profile', 'email']
    }));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect : '/catalog',
            failureRedirect : '/'
        }));

   	// =====================================
    // LOAD USER ===========================
    // =====================================
    app.get('/loaduser', (req, res) => {
    	res.send(req.user);
    });

};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
};
"use strict"
// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var scoreSchema = mongoose.Schema({

    user : String,
    score : Number

});

// create the model for users and expose it to our app
module.exports = mongoose.model('Score', scoreSchema);

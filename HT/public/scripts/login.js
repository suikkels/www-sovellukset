"use strict";

Vue.component('error-text', {
  template: '\
  	<div>\
  	 	{{ title }}\
  	 </div>\
  	',
  	 props: ['title']
});

var loginapp = new Vue({
	el: '#loginapp',

	data: {
		email: '',
		password: '',
		error: ''
	},
	mounted() {
    	console.log("logging in...");
  	},

	methods: {
		// POST request for login
		login(){
			axios.post('/login', {email : loginapp.email, password : loginapp.password})
			.then(function (response) {
				console.log(response.data);
				if(response.data.success){
					loginapp.error = '';
					window.location.href = "/catalog"; //redirect to /catalog
				} else {
					loginapp.error = "Wrong email or password"; //error
				}
			})
			.catch(function (error) {
				console.log(error);
			});
		},
	}
});

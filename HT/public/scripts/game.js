"use strict";

var myChart;
var ctx = document.getElementById("myChart").getContext('2d'); //for chart.js

var gameapp = new Vue({
    el: '#gameapp',

    data: {
        score: 0,
        user: '',
        topten: []
    },
    mounted() {
        // update current user & topten list
        this.getUser();
        this.updateList();
    },

    methods: {
        // POST request for new score to be added in db
        saveScore() {
            axios.post('/addscore', {user : gameapp.user, score : gameapp.score})
            .then(function (response) {
                console.log(response.data);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
        },

        getUser() {
            axios.get('/loaduser')
            .then(function (response) {
                console.log(response.data);
                if(response.data.facebook != undefined) {
                    gameapp.user = response.data.facebook.name;
                } else {
                    gameapp.user = response.data.local.email;
                }
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
        },

        updateList() {
            axios.get('/loadscore')
            .then(function (response) {
                console.log(response);
                gameapp.topten = [];
                for (var i = 0; i < 10; i++) {
                    // check if database contains data
                    if(response.data[i] != undefined){
                        gameapp.topten.push(response.data[i]);
                    } else {
                        // else create "nouser"-item
                        gameapp.topten.push({user : 'nouser', score : 0});
                    }
                }
                myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: [gameapp.topten[0].user, gameapp.topten[1].user, gameapp.topten[2].user, gameapp.topten[3].user, gameapp.topten[4].user, gameapp.topten[5].user, gameapp.topten[6].user, gameapp.topten[7].user, gameapp.topten[8].user, gameapp.topten[9].user],
                        datasets: [{
                            label: 'Score',
                            data: [gameapp.topten[0].score, gameapp.topten[1].score, gameapp.topten[2].score, gameapp.topten[3].score, gameapp.topten[4].score, gameapp.topten[5].score, gameapp.topten[6].score, gameapp.topten[7].score, gameapp.topten[8].score, gameapp.topten[9].score,],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                })

            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
        },
    }
});


var game = new Phaser.Game(800, 600,
    Phaser.CANVAS,
    'myCanvas',
    { preload: preload, create: create, update: update}
    );

function preload() {
    // load assets
	game.load.image('player', 'assets/player.png');
    game.load.image('bullet', 'assets/bullet.png');
    game.load.image('plant', 'assets/plant.png');

}

var sprite;
var bullets;
var cursors;
var plants;

var lives = 1;
var score = 0;
var counter = 0;
var bulletTime = 0;
var bullet;

var livesText;
var text;
var gameOverText;

function create() {
    //scaling options
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

    // set background
    game.stage.backgroundColor = '#2d2d2d';

    // add text for score and lives
    text = game.add.text(10, 10, "Score: 0", {
        font: "65px Arial",
        fill: "#ff0044",
        align: "center"
    });
    // text for lives
    livesText = game.add.text(game.world.width -250, 10, "Lives: 1", {
        font: "65px Arial",
        fill: "#ff0044",
        align: "center"
    });

    //add new groups for plant and bullet sprites
    plants = game.add.group();
    plants.enableBody = true;
    plants.physicsBodyType = Phaser.Physics.ARCADE;

	bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;

    // initialize bullet group
    for (var i = 0; i < 20; i++)
    {
        var b = bullets.create(0, 0, 'bullet');
        b.name = 'bullet' + i;
        b.exists = false;
        b.visible = false;
        b.checkWorldBounds = true;
        b.events.onOutOfBounds.add(resetBullet, this);
    }

    // create player sprite
    sprite = game.add.sprite(400, 500, 'player');
    sprite.enableBody = true;
    game.physics.enable(sprite, Phaser.Physics.ARCADE);

    // define keyboard as game cursor
    cursors = game.input.keyboard.createCursorKeys();
    game.input.keyboard.addKeyCapture([ Phaser.Keyboard.CONTROL ]);
}

function update() {
 //  As we don't need to exchange any velocities or motion we can the 'overlap' check instead of 'collide'
    game.physics.arcade.overlap(bullets, plants, collisionHandler, null, this);
    game.physics.arcade.overlap(sprite, plants, collisionHandler2, null, this);
    // create plant when random interval reaches 0
    if(counter == 0) {
        createPlant();
    }
    // iterate interval
    counter = counter -1;
    
    // stationary player velocity 0
    sprite.body.velocity.x = 0;
    sprite.body.velocity.y = 0;

    // else
    if (cursors.left.isDown)
    {
        sprite.body.velocity.x = -300;
    }
    else if (cursors.right.isDown)
    {
        sprite.body.velocity.x = 300;
    }
    // fire with control
    if (game.input.keyboard.isDown(Phaser.Keyboard.CONTROL))
    {
        fireBullet();
    }
}

function fireBullet () {

    if (game.time.now > bulletTime)
    {
        bullet = bullets.getFirstExists(false);

        if (bullet)
        {
            bullet.reset(sprite.x, sprite.y - 8);
            bullet.body.velocity.y = -300;
            bulletTime = game.time.now + 150;
        }
    }

}

//  Called if the bullet goes out of the screen
function resetBullet (bullet) {

    bullet.kill();

}

//  Called if the bullet hits one of the plant sprites
function collisionHandler (bullet, plant) {

    bullet.kill();
    plant.kill();
    updateScore();

}

//  Called if plant hits the player
function collisionHandler2 (player, plant) {

    player.kill();
    updateLives();

}
// create plants with random interval: 0-50
function createPlant() {
    var c = plants.create(game.world.randomX, -100, 'plant');
    c.name = 'plant';
    c.body.immovable = true;
    c.body.velocity.y = 300;
    counter = Math.ceil(Math.random() * 50);
}

function updateScore() {

    score = score + 100;

    text.setText("Score: " + score);

}

function updateLives() {
    
    lives = lives - 1;

    livesText.setText("Lives: " + lives);

    if(lives == 0) {
        // save score to gameapp
        gameapp.score = score;
        gameapp.saveScore();
        gameOverText = game.add.text(game.world.centerX-400, game.world.centerY/2, "Game Over\nRefresh page to play again!", {
            font: "65px Arial",
            fill: "#ff0044",
            align: "center"
        });
    }

}
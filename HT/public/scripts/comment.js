"use strict";
// create template for comment
Vue.component('comment-item', {
  template: '\
  	<div class="row">\
    	<div class="col s12 col-offset-3">\
      		<div class="card-panel teal lighten-2">\
	      		<div class="card-content white-text">\
	       			<span class="card-title">\
	        			{{user}} says:\
	       			</span>\
	        		<p>\
						{{comment}}\
	        		</p>\
	        	</div>\
      		</div>\
    	</div>\
  	</div>\
  	 ',
  	 props: ['user', 'comment']
});

var commentapp = new Vue({
	el: '#commentapp',

	data: {
		newCommentText: '',
		currentUser: '',
		comments: [],
	},

	mounted() {
		// load current user & update comments 
		this.getUser();
    	this.updateComments();
  	},

	methods: {
		comment(){
			console.log(this.newCommentText);
			// send POST request for new comment
			axios.post('/addcomment', {user : commentapp.currentUser, text : commentapp.newCommentText})
			.then(function (response) {
				console.log(response);
			})
			.catch(function (error) {
				console.log(error);
			});
			// update new comments
			this.updateComments();
		},

		updateComments() {
			axios.get('/loadcomments')
			.then(function (response) {
				// initialize comment array
				commentapp.comments = [];
				for (var i = 0; i < response.data.length; i++) {
					// push response data to comment array
					commentapp.comments.push(response.data[i]);
				}
				console.log(commentapp.comments);

			})
			.catch(function (error) {
				// handle error
				console.log(error);
			})
		},

		getUser() {
			axios.get('/loaduser')
            .then(function (response) {
                console.log(response.data);
                // check for facebook authentication
                if(response.data.facebook != undefined) {
                    commentapp.currentUser = response.data.facebook.name;
                } else {
                	// else local
                    commentapp.currentUser = response.data.local.email;
                }
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
		}
	}

});

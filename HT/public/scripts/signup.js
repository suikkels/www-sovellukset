"use strict";

Vue.component('error-text', {
  template: '\
  	<div>\
  	 	{{ title }}\
  	 </div>\
  	',
  	 props: ['title']
});


var signapp = new Vue({
	el: '#signapp',

	data: {
		email: '',
		password: '',
		error: ''
	},
	mounted() {
    	console.log("signing up");
  	},

	methods: {
		// POST request for signup
		signup(){
			axios.post('/signup', {email : signapp.email, password : signapp.password})
			.then(function (response) {
				console.log(response.data);
				if(response.data.success){
					//if successful
					signapp.error = '';
					window.location.href = "/catalog"; //redirect to /catalog
				} else {
					signapp.error = "email already in use"; //error message
				}
			})
			.catch(function (error) {
				console.log(error);
			});
		},
	}
});
"use strict";

Vue.component('user-item', {
  template: '\
  	<h2 class="center">\
  		Welcome {{ title }}!\
  	</h2>\
  	 ',
  	 props: ['title']
});

var userapp = new Vue({
	el: '#userapp',

	data: {
		currentUser: '',
	},

	mounted() {
    	this.updateUser();
  	},

	methods: {
		// get current user
		updateUser() {
			axios.get('/loaduser')
			.then(function (response) {
				console.log(response.data);
				if(response.data.facebook != undefined) {
					userapp.currentUser = response.data.facebook.name;
				} else {
					userapp.currentUser = response.data.local.email;
				}
			})
			.catch(function (error) {
				// handle error
				console.log(error);
			})
		},
	}

});

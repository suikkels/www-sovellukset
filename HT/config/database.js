"use strict"
module.exports = {
	// for actual app
    'url' : 'mongodb://mongo:27017/HT',
    // for unit testing
    'uniturl' : 'mongodb://localhost:27017/HT'
};

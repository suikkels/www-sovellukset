"use strict"
module.exports = {

    'facebookAuth' : {
        'clientID'      : '2401044193243528', // your App ID
        'clientSecret'  : '6e63504928271d854f58d91c325e28ca', // your App Secret
        'callbackURL'   : 'http://localhost:3000/auth/facebook/callback',
        'profileURL'    : 'https://graph.facebook.com/v2.5/me?fields=first_name,last_name,email',
        'profileFields' : ['id', 'email', 'name'] // For requesting permissions from Facebook API
    },
};
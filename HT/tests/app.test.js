const Promise = require('bluebird');
const request = require('supertest');
const localmongoose = require('mongoose');

localmongoose.Promise = Promise;
const app = require('../app');
const user = require('../app/models/user.js');
var configDB = require('../config/database.js');

beforeAll(() => {
  localmongoose.connect(configDB.uniturl);
});

afterAll(() => {
  localmongoose.disconnect();
});

describe('Test GET comments ', () => {
  it('Test if responds with json', (done) => {
    request(app).get('/loadcomments')
    .expect('Content-Type', 'application/json; charset=utf-8')
    .expect(200, done);
  });
});

describe('Test POST signup ', () => {
  it('Test if signup successful', (done) => {
    request(app).post('/signup')
    .send({email : 'lollero@asd.foo', password : 'asd'})
    .expect(function(res) {
      res.body.user.local.email = res.body.user.local.email;
      res.body.user.local.password = 'some hashed password';
      res.body.user._id = 'some id';
      res.body.user.__v = res.body.user.__v;
      res.body.success = res.body.success;
      res.body.message = res.body.message;
    })
    .expect(200, {
      
      user : {__v : 0, _id : 'some id', local : {password : 'some hashed password', email : 'lollero@asd.foo'}},
      success: true,
      message: 'signup successful'
    }, done);
  });
});

describe('Test POST score ', () => {
  it('Test if responds with sent user and score', (done) => {
    request(app).post('/addscore')
    .send({user : 'lollero', score : 222})
    .set('Accept', 'application/json')
    .expect(function(res) {
      res.body.score = res.body.score;
      res.body._id = 'some id';
      res.body.__v = 0;
      res.body.user = res.body.user;
    })
    .expect(200, {
      __v: 0,
      _id: 'some id',
      score: 222,
      user: 'lollero'
    }, done);
  });
});

describe('Test root path', () => {
  test('Test if responds with html', (done) => {
    request(app).get('/')
    .expect('Content-Type', 'text/html; charset=utf-8')
    .expect(200)
    .then(response => {
      done();
    });
  });
});


describe('Test Mongoose database connection', () => {
  test('Test connection', (done) => {
    localmongoose.connect('mongodb://localhost:27017/HT').then(() => {
      expect(localmongoose.connection.readyState).toBe(1);
    });
    localmongoose.disconnect();
    done(); // eslint-disable-line no-undef
  });
});


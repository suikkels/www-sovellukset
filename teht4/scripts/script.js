Vue.component('list-item', {
  template: '\
  	<li class="collection-item">\
  	 <div>{{ title }}<a href = "#!" class="secondary-content">\
  	 <i class="material-icons icon-red" v-on:click="$emit(\'remove\')">clear</i></a></div>\
  	 </li>\
  	 ',
  	 props: ['title']
});

var vm = new Vue({
	el: '#app',

	data: {
		newItemText: '',
		items: [],
		nextItemId: 1
	},
	mounted() {
    	if (localStorage.newItemText) {
      		this.newItemText = localStorage.newItemText;
    	}
    	if(localStorage.getItem('items')) {
    		try{
    			this.items = JSON.parse(localStorage.getItem('items'));
    		} catch(e) {
    			localStorage.removeItem('items');
    		}
    	}
  	},
  	watch: {
    	newItemText(newNewItemText) {
      		localStorage.newItemText = newNewItemText;
    	},
  	},
	methods: {
		addItem: function (){
			if(this.items.length != 0) {
				for(var i = 0; i<this.items.length; i++) {
					if(this.items[i].id == this.nextItemId) {
						this.nextItemId++;
					}
				}
			}
			if (!this.validText(this.newItemText)) {
				return 0;
			} else {
				this.items.push({
					id: this.nextItemId++,
					title: this.newItemText

				})
				this.saveItems();
			}
			this.newItemText = ''
		},

		validText: function(itemText){
			var re = /^([a-zA-Z\-0-9\-,.]+)$/;
			return re.test(itemText);
		},

		saveItems: function(){
			const parsed = JSON.stringify(this.items);
			localStorage.setItem('items', parsed);
		}
	}

});
